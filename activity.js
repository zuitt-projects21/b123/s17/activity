let registeredUsers = [
"jamieJamie21",
"45_gunther",
"KingOfTheWest_2003",
"freedomFighter47",
"kennethTheKnight",
"theScepter07",
"ninjaisme00"
];

let friendsList = [];

//function 1
function register(user){

	let isRegistered = registeredUsers.includes(user);

	if(isRegistered === true){
		alert("Registration failed. Username already exists!")
	} else{
		registeredUsers.push(user);
		alert("Thank you for registering!");
	};

};

//function 2
function addFriend(friend){

	let isRegistered = registeredUsers.includes(friend);

	if(isRegistered === true){
		friendsList.push(friend);
		alert(`You have added ${friend} as a friend.`);
	} else{
		alert("User not found!");
	};

};


//function 3
function displayFriends(){
	if(friendsList.length == 0){

		alert("You currently have 0 friends. Please add one first.")

	} else{

		friendsList.forEach(function(friend){
		console.log(friend);
		});

	};

	
};

//function 4
function displayNumberOfFriends(){

	if(friendsList.length == 0){

		alert("You currently have 0 friends. Please add one first.")

	} else{
		let numFriends = friendsList.length;
		alert(`You currently have ${numFriends} friends.`);
	};

};
//function 5
function deleteFriend(friend){

	if (friend !== ""){
		let deleteIndex = friendsList.indexOf(friend);
		let deletedFriend = friendsList[deleteIndex];
		alert(`You have removed ${deletedFriend} from your friends list.`);
		friendsList.splice(deleteIndex, 1);
	} else{
		alert(`You have removed the last friend from your friends list.`);
		friendsList.pop();
	}
	//note: all other inputs deletes the last friend in list. yet to include a working checker if input is on friends list or not.
};

//alternative code

function deleteFriend1(friend){

	let isFriend = friendsList.includes(friend);
	let isInputNull;
	
	if (friend === null){
		isInputNull = true;
	} else{
		isInputNull = false;
	};

	if (isInputNull === false && isFriend === true){
		let deleteIndex = friendsList.indexOf(friend);
		let deletedFriend = friendsList[deleteIndex];
		alert(`You have removed ${deletedFriend} from your friends list.`);
		friendsList.splice(deleteIndex, 1);
	} else if(isInputNull === false && isFriend === false){
		alert("Cannot delete. Friend does not exist.");
	} else{
		alert(`You have removed the last friend from your friends list.`);
		friendsList.pop()
	};
	//still does not recognize null argument. 
}
